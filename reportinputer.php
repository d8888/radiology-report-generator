﻿<!--
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
-->
<?php
	function TimeStamp()
	{
		//This forces a timestamp after javascript file to prevent caching of JS files.
		if(!isset($_GET["debug"]))
		{
			echo "?time=".time();
		}else
		{
			echo "?time=".$_GET["debug"];
		}
	}
?>
<head>
<title>觸控式報告輸入裝置 ver 0.01 by d8888</title>
<link rel="stylesheet" type="text/css" href="style.css">
<!--<link rel="stylesheet" type="text/css" href="colors.css">-->
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<script src="jquery.js"></script> 
<script src="jquery.base64.js"></script>
<script src="ErrObj.js<?php TimeStamp();?>"></script> 
<script src="basefunc.js<?php TimeStamp();?>"></script> 
<script src="controller.js<?php TimeStamp();?>"></script> 
<script src="misc.js<?php TimeStamp();?>"></script> 
<script>
/*
	Test template
*/

/*
	Code
*/
$(document).ready(function()
{
	ResizeAll();
}); 

//$(document).ready(ResizeAll());    // When the page first loads
$(window).resize(ResizeAll());     // When the browser changes size

function CallLoadMacro(e)
{
	e.preventDefault();
	
	LoadMacro();
	
	if(MyDict)
	{
		InitControls();
	}
}

function CallUndo(e)
{
	e.preventDefault();
	
	Undo();
}

function CallSendReport(e)
{
	e.preventDefault();
	
	SendReport();
}

function NextPage(e)
{
	e.preventDefault();
	
	SwitchPage(1);
}

function PrevPage(e)
{
	e.preventDefault();
	
	SwitchPage(-1);
}

/*
	Prevent dragging
*/
function preventPanning(event)
{
	event.preventDefault();
}

</script>
</head>
<body>
	<div id="screen" ontouchmove="preventPanning(event);"> 
		<form>
			<div id="container">
				<div id="content">
					<div class="UserInput" id="UserInput">
						使用者名稱：<input type="text" name="username" id="username"><br>
						密碼：<input type="password" name="pwd" id="pwd"> 二：<input type="password" name="pwd2" id="pwd2" size="5"><br>
						巨集名稱：<input type="text" name="macroname" id="macroname"><br>
						顯示項目：<input type="text" name="macrotitle" id="macrotitle"><br>
					</div>
					<textarea name="report" id="report"> </textarea>
					<div id="MessageArea"> &nbsp;</div>
				</div>
				<div id="sidebar">
					<div id="ButtonControls">
					</div>
				</div>
			</div>
			<div id="footer">
				<div id="PageSwitcher">
				<button type="button" ontouchstart="CallLoadMacro(event)" onclick="CallLoadMacro(event)">載入</button>
				<button type="button" ontouchstart="NextPage(event)" onclick="NextPage(event)">下頁</button>
				<button type="button" ontouchstart="PrevPage(event)" onclick="PrevPage(event)">上頁</button>
				<button type="button" ontouchstart="CallUndo(event)" onclick="CallUndo(event)">上步</button>
				<button type="button" ontouchstart="CallSendReport(event)" onclick="CallSendReport(event)">送出</button>
				</div>
			</div>
		</form>
	</div>
</body>