<head>
<title>新增使用者</title>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
</head>
<?php
/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
	require_once("misc.php");

	//var_dump($_POST);
	

	
	
	if(isset($_POST["username"]) && isset($_POST["pwd"]))
	{
		if(strcmp($_POST["pwd"], $_POST["confirmpwd"])!==0)
		{
			echo "密碼輸入不一致！";
			exit(0);
		}
		
		if(!IsUsernameAvailable($_POST["username"]))
		{
			echo "使用者名稱已被使用！";
			exit(0);
		}
		
		if(AddUser($_POST["username"], $_POST["pwd"]))
		{
			$usr = $_POST["username"];
			echo <<<MESSAGE
使用者新增成功<bR>
請用記事本等文書軟體打開 d8888webreport.ahk<bR>
用以下內容將原本檔案 POSTData := "username= ooxxooxx" 那行蓋掉後存檔<br>
<br>
POSTData := "username=$usr"

MESSAGE;
			exit(0);
		}

	}
?>

<body>
	<form method="post">
	
		<div>
			使用者名稱：<input type="text" name="username"><br>
			密碼：<input type="password" name="pwd"><br>
			確認密碼：<input type="password" name="confirmpwd"><br>
		</div>
		<input type="submit" value ="送出">
	</form>
</body>