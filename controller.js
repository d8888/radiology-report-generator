/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


/*
	Configs
*/


/*
	Global objects
*/

var GUILayers = [];
var LastReport = "";

var GlobalReportText = "";

var IsWaitingRemoteCmd = false;



/*
	Controller
*/

var MyDict;

function Undo()
{
	if(GUILayers.length == 0)
	{
		return;
	}
	GUILayers[GUILayers.length - 1].Undo();
}


/*
	aaa
	bbb
	ccc
	
	--> 
	
	Aaa
	Bbb
	Ccc
	
	before send out
	
	this is a pen --> This is a pen.
*/
function PostProcess(text)
{
	var splittext= text.split("\n");
	var rst = "";
	
	for(var key in splittext)
	{
		line = splittext[key];
		if(line.length>0)
		{
			var newline = line.charAt(0).toUpperCase() + line.slice(1);
			

			
			if(rst!="")
			{
				rst = rst + "\n";
			}
			rst = rst + newline;
		}
	}
	return rst;
}

function AlertMessage(text)
{
	var dt = new Date();
	var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
	$("#MessageArea").text(time+" "+text);
}


function WaitRemoteCmd()
{
	/*
		wait remote command
	*/

	if(IsWaitingRemoteCmd)
	{
		return;
	}
	
	IsWaitingRemoteCmd = true;
	
	$.ajax(
	 {
		url: "getremotecmd.php",
		data: 
		{
			username: $("#username").val(),
			token: $("#pwd2").val()
		},
		type: "GET",
		/*dataType: "json",*/
		success: function(data,textStatus,jqXHR) 
		{
			IsWaitingRemoteCmd = false;
			ExecRemoteCmd(data);
			setTimeout(WaitRemoteCmd, 200);
		},
		error: function(data, errorThrown)
		{
			IsWaitingRemoteCmd = false;
			setTimeout(WaitRemoteCmd, 200);
		},
		timeout: 150000
	} 
	);
	
}

function ExecRemoteCmd(data)
{
	
	var aray= data.split("###");
	
	cmd = aray[0];
	para = aray[1];
	
	if(cmd == "open")
	{
		InitControlsByText(para);
	}
	return;
}

function SendReport()
{
	var SentTarget = GetFormattedText(GlobalReportText);
	SentTarget = PostProcess(SentTarget);
	
	if(SentTarget.length <=1)
	{
		if(LastReport!="")
		{
			AlertMessage("報告內容為空，改為先前報告重新傳送");
			SentTarget = LastReport;
		}else
		{
			return;
		}
	}
	LastReport = SentTarget;
	
	$.ajax(
	 {
		url: "setreport.php",
		data: 
		{
			username: $("#username").val(),
			report: $.base64.encode(SentTarget),
			pwd: $("#pwd").val()
		},
		type: "POST",
		/*dataType: "json",*/
		success: function(data,textStatus,jqXHR) 
		{
			WipeCtrls();
			SetReportText("");
		},
		error: function(data, errorThrown)
		{
			AlertMessage("報告傳送錯誤："+errorThrown);
		},
		complete: function() 
		{
			WipeCtrls();
			SetReportText("");
		}
	} 
	);
}

function InitControlsByText(text)
{
	WipeCtrls();
	GUILayers=[];
	GUILayers.push(new LayerObj());
	GUILayers[GUILayers.length-1].CreateControls(text,0);
}

function InitControls()
{
	InitControlsByText($("#macrotitle").val());
}

function LoadMacro()
{
	RequestRemoteMacro($("#username").val(), $("#pwd").val(), $("#macroname").val(), InitDict, AlertMessage);
	
	/*
		Wait for remote command
	*/
	WaitRemoteCmd();
}






function InitDict(dictstr)
{
	var rstobj={};
	if(!InitDictByStr(dictstr, rstobj))
	{
		msg="";
		for(var key in ErrObj)
		{
			msg=msg+ ErrObj[key]["errmsg"]+"\n";
		}
		AlertMessage(msg);
		return false;
	}
	
	MyDict = jQuery.extend(true, {}, rstobj.dictobj);
	
	return true;
}

/*

// Function to automatically split long pages
// Commented out due to bugs

function SliceDict()
{
	var MaxLength = 9;
	
	for(var key in MyDict)
	{
		aray=[];
		for(var page in MyDict[key])
		{
			aray.push([]);
			for(var item in MyDict[key][page])
			{
				aray[aray.length-1].push(MyDict[key][page][item]);
				if(aray[aray.length-1].length >= MaxLength)
				{
					aray.push([]);
				}
			}
		}
		

		if(aray[aray.length-1].length ==0)
		{
			aray.slice(aray.length-1,1);
		}
		MyDict[key]=aray;
	}
	return;
}
*/


function WipeCtrls()
{
	$("#ButtonControls").empty();
}



function SetReportText(text)
{
	GlobalReportText = text;
	
	$("#report").val(GetFormattedText(GlobalReportText));
	$('#report').scrollTop($('#report')[0].scrollHeight);
}

function GetReportText()
{
	return GlobalReportText;
}

function AppendReportText(text)
{
	SetReportText(GetReportText() + text);
}

function AppendText(text)
{
	GUILayers[GUILayers.length -1].AppendText(text);
}

function ButtonAppendText(e, text)
{
	e.preventDefault();
	
	AppendText(decodeURI(text));
}

function AddButton(caption, newtext)
{
	var encodedtext = encodeURI(newtext);
	button = "<button type=\"button\" ontouchstart=\"ButtonAppendText(event,\'"+encodedtext+"\')\""+
	" onclick=\"ButtonAppendText(event,\'"+encodedtext+"\')\">"+caption+"</button><br>";
	$("#ButtonControls").append(button);
}

function RemoveTail(text)
{
	//remove trailing comma or concat operator
	if(text.length>0)
	{
		var last = text[text.length-1];
		if(last == commaoperator || last == concatoperator)
		{
			text = text.slice(0,text.length-1);
		}
	}
	return text;
}

function SwitchPage(dir)
{
	pnum = GUILayers[GUILayers.length - 1].GetCurrentPage();
	tar = GUILayers[GUILayers.length - 1].GetCurrentTarget();
	if(GUILayers[GUILayers.length - 1].CreateControls(tar, pnum+dir)==false)
	{
		if(GUILayers.length == 1)
		{
			// To be implemented:: send result
			// alert(GetReportText());
			alert("end of page")
			return;
		}else
		{
			s = RemoveTail(GUILayers[GUILayers.length-1].GetResultText());
			s = RStripTabWhite(s);
			GUILayers[GUILayers.length - 1].Close();
			GUILayers.pop();
			//Return to parent layer
			GUILayers[GUILayers.length - 1].Redraw();
			GUILayers[GUILayers.length - 1].RestoreSavedText();
			ReplaceFormWith(s, true);
		}
	}
	return;
}

function ReplaceFormWith(text, savefilledword)
{
	GUILayers[GUILayers.length - 1].FillSubForm(text, savefilledword);
}


function GetFormattedText(text)
{
	/*
		Split report into sections
		e.g
		
		A [Impression===a]
		
		into
		
		A
		
		Impression:
		a
	*/
	
	var RegEx = /\[[^\[\]]*\]/
	
	var SecAray = [];
	
	text = RemoveTail(text);
	
	while(1)
	{
		var m = RegEx.exec(text);
		if(m)
		{
			sp = m[0].slice(1, m[0].length-1); //Get rid of []
			t=sp.split("===");
			
			if(!(t[0] in SecAray))
			{
				SecAray[t[0]] = [];
			}
			SecAray[t[0]].push(t[1]);
		
			text = text.replace(RegEx, "");
		}else
		{
			break;
		}
	}
	
	for(key in SecAray)
	{
		text = text + "\n" + key + ":";
		for(index in SecAray[key])
		{
			text = text + "\n" + SecAray[key][index];
		}
	}
	return text;
}



function LayerObj()
{
	/*
		Private members
	*/
	var __PageNum = 0;
	var __Target = "";
	var __SavedRst = "";
	var RegEx = /\{[0-9a-zA-Z@]*\}/
	
	var __UndoStack = [];
	var __UndoConStack = [];

	var __ConOP = "";
	var __LastAray = [];
	
	/*
		Private Functions
	*/
	function __KillOldCtrl()
	{
		WipeCtrls();
	};


	function __CheckCreateSubForm()
	{
		var g = __GetResultText();
		var m = RegEx.exec(g);
		if(!m)
		{
			return;
		}
		
		sp = m[0].slice(1, m[0].length-1); //Get rid of { }
		
		/*
			@Copy operator
		*/
		if(__CheckCopy(sp))
		{
			return;
		}

		__SavedRst = g;
		__SetResultText("", false);
		
		GUILayers[GUILayers.length-1].Close();
		GUILayers.push(new LayerObj());
		GUILayers[GUILayers.length - 1].CreateControls(sp,0);
		
	}

	function __CheckCopy(sp)
	{
		n = __LastAray.length;
		
		ret=false;
		
		if(sp == "@COPY")
		{
			if(n>0)
			{
				ReplaceFormWith(__LastAray[n-1],false);
				ret=true;
			}
		}else if(sp == "@COPY2")
		{
			if(n>1)
			{
				ReplaceFormWith(__LastAray[n-2],false);
				ret=true;
			}
		}else if(sp == "@COPY3")
		{
			if(n>2)
			{
				ReplaceFormWith(__LastAray[n-3],false);
				ret=true;
			}
		}else if(sp == "@COPY4")
		{
			if(n>3)
			{
				ReplaceFormWith(__LastAray[n-4],false);
				ret=true;
			}
		}
	
		return ret;
	}
	
	function __SaveNow()
	{
		__UndoStack.push(__GetResultText());
		__UndoConStack.push(__ConOP);
	}
	
	function __AppendResultText(text)
	{
		//__SaveNow();
		AppendReportText(text);
	}
	function __DelLastChar(savenow)
	{
		var now = GetReportText();
		if(now.length>0)
		{
			if(savenow)
			{
				__SaveNow();
			}
			SetReportText(now.slice(0,now.length-1));
		}
		return;
	}

	
	function __GetResultText()
	{
		return GetReportText();
	}
	function __SetResultText(newtext, saveundo)
	{
		if(saveundo == true)
		{
			__SaveNow();
		}
		SetReportText(newtext);
	}
	
	/*
		Public functions
	*/
	this.Yo = function()
	{
		alert("yoooo");
	}
	
	this.Undo = function()
	{
		if(__UndoStack.length == 0)
		{
			return;
		}
		__SetResultText(__UndoStack[__UndoStack.length-1], false);
		__UndoStack.pop();
		__ConOP = __UndoConStack[__UndoConStack.length-1];
		__UndoConStack.pop();
	}
	this.FillSubForm = function(text, savefilledword)
	{	
		__SaveNow();
	
		if(savefilledword)
		{
			__LastAray.push(text);
		}
		
	
		var g = __GetResultText();
		var m = RegEx.exec(g);
		
		if(m)
		{
			var newrst = g.replace(RegEx, text);
			__SetResultText(newrst, false);
		}else
		{
			if(__GetResultText().length>0)
			{
				text = "\n" + text;
			}
			__AppendResultText(text);
		}
	
		__CheckCreateSubForm();
	}
	this.GetResultText = function()
	{
		return __GetResultText();
	}
	
	this.AppendText = function(text)
	{
		__SaveNow();
		var g = __GetResultText();
		
		text = RStripTabWhite(text);
	
		
		var now = GetReportText();
		if(now.length>0)
		{
			var last = now[now.length-1];
			if(last == commaoperator)
			{
				text = " " + text;
			}else if(last == concatoperator)
			{
				__DelLastChar(false);
				text = " " + text;
			}else
			{
				text = "\n" + text;
			}
		}
		__AppendResultText(text);
		__CheckCreateSubForm();
		return;
	}
	this.Close = function()
	{
		__KillOldCtrl();
	}
	this.Redraw = function()
	{
		this.CreateControls(__Target, __PageNum);
	}
	this.GetCurrentPage = function()
	{
		return __PageNum;
	}
	this.GetCurrentTarget = function()
	{
		return __Target;
	}
	this.RestoreSavedText = function()
	{
		__SavedRst = RStripTabWhite(__SavedRst); //Hack
		__SetResultText(__SavedRst, false);
		
	}
		
	this.CreateControls = function(target,pagenum)
	{
		// clear old controls
		__KillOldCtrl();
		// Create new ones
		__PageNum = pagenum;
		__Target= target
		
		if(!(__Target in MyDict))
		{
			AlertMessage("名稱不存在："+target+"  請檢查拼寫有無錯誤。");
		}
		
		mytemp = MyDict[__Target];
		if(pagenum >= mytemp.length)
		{
			return false;
		}
		if(mytemp[pagenum].length==0)
		{
			return false;
		}
			
		for(i in mytemp[pagenum])
		{
			var caption="";
			var newtext="";
			var testvar2;
			var linestr;
			linestr = mytemp[pagenum][i];
			
			testvar2=linestr.split(delimiter);
			
			if(testvar2.length == 1)
			{
				caption=testvar2[0];
				newtext=caption;
			}else if(testvar2.length == 2)
			{
				caption=testvar2[0];
				newtext=testvar2[1];
			}else
			{
				AlertMessage("語法錯誤："+linestr);
				return false;
			}
			
			
			caption = StripTabWhite(caption);
			newtext = StripTabWhite(newtext);

			/*
				TEXT::description is no longer implemented. 
				It is not removed for compatability reason.
			*/
			if(caption.length >0 && caption!="TEXT")
			{
				AddButton(caption, newtext);
			}

		}
		return true;
	}
	
}



/*
	Global init
*/

