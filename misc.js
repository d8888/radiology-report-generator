/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

function ResizeAll()
{
    // Get the dimensions of the viewport
	
	var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

    var width = x;
    var height = y;


	
	$("#container").height(height*0.8);
	$("#container").width(width);

	
	$("#footer").height(height*0.1);

	
	$("#content").width(width*0.6);
	$("#content").height($("#container").height());
	
	$("#sidebar").width(width*0.4);
	
	

	$("#report").height($("#content").height() - $("#UserInput").height() - $("#MessageArea").height());
	$("#report").width($("#content").width());
	
}

