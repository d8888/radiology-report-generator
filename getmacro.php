<?php
/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
	$ReadMacroAray=array(); //Used by ReadMacro to prevent cycling when including macro
	
	function ReadMacro($fname)
	{
		global $ReadMacroAray;
		if(isset($ReadMacroAray[$fname]))
		{
			Show("讀取".$fname."時發現 cycle！");
			return;
		}
		$ReadMacroAray[$fname]=true;
		
		$openname = "files/".md5($_POST["username"].$fname);
		
		$handle = fopen($openname, "r");
		if($handle == FALSE)
		{
			Show("Open file fail:".$fname." count:".strlen($fname)." name:".$openname);
			return false;
		}else
		{
			$rst = fread($handle, filesize($openname));
			fclose($handle);
			
			if($rst == FALSE)
			{
				Show("Read fail:".$fname);
				return false;
			}else
				
			
			$lines = explode("\n", $rst);
			$linecount=count($lines);
			$retval="";
			
			for($i=0;$i<$linecount;$i++)
			{
				$temp=$lines[$i];
			
				
				$tmparay = explode("FILE::",$temp);
				if(count($tmparay)==2)
				{
					$v = ReadMacro(trim($tmparay[1]));
					$retval = $v."\n".$retval;
				}else
				{
					$retval = $retval.$temp."\n";
				}
			}	
			return $retval;
		
		}
		return false;
	}
	require_once("misc.php");

	/*
		need:
			macroname
			username
		as $_POST parameter
	*/
	function Show($txt)
	{
		echo json_encode($txt);
	}
	
	
	if(isset($_POST["macroname"])&&isset($_POST["username"])&&isset($_POST["pwd"]))
	{
		if(!AuthUser($_POST["username"], $_POST["pwd"]))
		{
			$rst = "##AUTHFAILED";
			Show($rst);
			return;
		}
		$rst="";		
		
		$rst=ReadMacro($_POST["macroname"]);
		
		
		if($rst == FALSE)
		{
			Show("檔案開啟失敗！");
		}else
		{
			Show($rst);
		}
		return;
	}
?>