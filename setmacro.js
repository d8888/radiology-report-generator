
function initeditor()
{
	//init of the scripts
	m=$("#macro");
	//m.bind("input propertychange",updatepreview());
	m.bind("input propertychange",updatepreview);
	
}
function AddLine(newline)
{
	old=$("#preview").html();
	old=old + "<br>" + newline;
	$("#preview").html(old);
}
function WipePreview()
{
	$("#preview").text("");
}

function updatepreview()
{
	/*
		目前此部分 parser 跟 controller.js 的 parser 分開實作
		若加入新語法則此部分也要更新
		未來也許可以考慮合併
	*/
	
	WipePreview();
	
	dictstr = $("#macro").val();
	rstobj=[];
	
	if(!InitDictByStr(dictstr, rstobj))
	{
		AddLine("發生內部錯誤，疑似巨集本身有問題");
		return;
	}
	MyDict = jQuery.extend(true, {}, rstobj.dictobj);
	
	var NowKey="";
	var splitdict= dictstr.split("\n");
	
	
	
	for(var key in splitdict)
	{
		line = splitdict[key]
		line = StripTabWhite(line);
		line = StripReturn(line);
		
			
		/*
			Is it a new key?
		*/
		robj = {};
		if(MatchNewKey(line, robj))
		{
			var NowKey = robj.keyname;
			AddLine("定義新項目："+ NowKey);
			continue;
		}

		
		/*
			Not new key
			skip if blank line
		*/
		if(line=="")
		{
			continue;
		}
		
		/*
			defining a subitem, however NowKey is blank
			there is bug in grammar!
		*/
		if(NowKey == "" )
		{
			var errmsg = "錯誤：不在項目下的子項目！ "+ line;
			AddLine(errmsg);
			continue;
		}
				
				
		b=IsInlineMacro(line, temp)
		if(b)
		{
			key = temp["str"];
			if(key in MyDict)
			{
				AddLine("插入項目："+key);
			}else
			{
				AddLine("錯誤！插入不存在的項目："+key);
			}
			continue;
		}
		
		/*
			Default cases
		*/
		if(!MatchPageDelimiter(line))
		{
			AddLine("子項目："+line);
		}else
		{
			AddLine("---換下一頁---");
		}
				
	}
	
	

	
	return true;
	
}