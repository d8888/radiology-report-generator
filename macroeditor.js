/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
/*
	Code for GUI macro editors
*/

/*

function WipeMacroItems()
{
	$("#macroitems").empty();
}


function LoadItem()
{
	var svalue = $("#macroitems").find(":selected").text();
	
	$("#itemcontent").empty();
	
	var second = false;
	var totalsize = 0;
	for(var page in MyDict[svalue])
	{
		if(second)
		{
			var nowvalue = PageDelimiter;
			var newop="<option value=\""+nowvalue+"\">"+nowvalue+"</option>";
			$("#itemcontent").append(newop);
			totalsize++;
		}
		for(var item in MyDict[svalue][page])
		{
			var nowvalue = MyDict[svalue][page][item];
			var newop="<option value=\""+nowvalue+"\">"+nowvalue+"</option>";
			$("#itemcontent").append(newop);
			totalsize++;
		}
		second = true;
	}
	$("#itemcontent").attr('size', totalsize);
}*/


/*
	Consts
*/
var TREEITEM_ITEM = 1;
var TREEITEM_PAGE = 2;
var TREEITEM_SUBITEM = 3;
var UPREFIX = "##!!U_";

var NewItemText = "NewItem";
var NewSubitemText = "NewSubItem";
var NewPageText = "NewPage";

/*
	Global objects
*/
var NowSelected;
var NowTree;
var GlobalId = 0;

/*
	Function 
*/

function getSortedKeys(obj) 
{
    var keys = []; 
	for(var key in obj) 
	{
		keys.push(key);
	}
    //return keys.sort(function(a,b){return obj[a]-obj[b]});
	return keys.sort();
}

function MyDictToTree()
{
	var RetVal=[];
	
	var SortedKeys = getSortedKeys(MyDict);
	
	for (var k in SortedKeys)
	{
		var item = SortedKeys[k];
		
		var nowitem = MyDict[item];
		
		var nowitemnode = CreateBlankItem();
		nowitemnode.text = item;
		
		for(var page in nowitem)
		{
			var nowpage = nowitem[page];
			
			var nowpagenode = CreateBlankPage();
			nowpagenode.text = page;
			nowpagenode.parentitem = item;
			
			for(var subitem in nowpage)
			{
				var nowsubitem = nowpage[subitem];
				
				var nowsubitemnode = CreateBlankSubitem();
				nowsubitemnode.text = nowsubitem;
				nowsubitemnode.parentitem = item;
				nowpagenode.children.push({});
				$.extend(nowpagenode.children[ nowpagenode.children.length - 1], nowsubitemnode)
			}
			nowitemnode.children.push({});
			$.extend(nowitemnode.children[ nowitemnode.children.length - 1], nowpagenode)
		}
		//RetVal.push(nowitemnode);
		RetVal.push({});
		$.extend(RetVal[ RetVal.length - 1], nowitemnode)
	}
	NowTree = RetVal;
	return;
}

function TreeToRaw()
{
	var RetVal = "";
	for(var itemiter in NowTree)
	{
		var ItemNode = NowTree[itemiter];
		RetVal = RetVal + "\n";
		RetVal = RetVal + StripTabWhite(ItemNode.text);
		RetVal = RetVal + delimiter;
		
		var firstpage = true;
		for(var pageiter in ItemNode.children)
		{
			var PageNode = ItemNode.children[pageiter];
			
			if(!firstpage && PageNode.children.length>0)
			{
				RetVal = RetVal + "\n" + PageDelimiter;
			}
			firstpage = false;
			
			for(var subitemiter in PageNode.children)
			{
				var SubitemNode = PageNode.children[subitemiter];
				RetVal = RetVal + "\n\t";
				RetVal = RetVal + StripTabWhite(SubitemNode.text);
			}
			
		}
	}
	return RetVal;
}

function GetItemNameOf(JsTreeNode)
{
	var t;
	if(JsTreeNode.original.type == TREEITEM_ITEM)
	{
		t = JsTreeNode.original.text;
	}else
	{
		t = JsTreeNode.original.parentitem;
	}
	return t;
}
function TreeSearchCallback(str, node)
{
	var t = GetItemNameOf(node);
	if(t.search(str)!=-1)
	{
		return true;
	}
	return false;
}

function CreateTheTree()
{
	$('#MacroTree').removeClass();
	$('#MacroTree').jstree(
		{ "core" : {
				'data' : NowTree,
			},
		"search" : {
			"show_only_matches" : true,
			"search_callback" : TreeSearchCallback
		},
		"plugins" : [ "search" ]
	});
}

function LoadMacroItems()
{
	MyDictToTree();
	CreateTheTree();
}

function UpdateUI()
{
	if(NowSelected.node.original.type == TREEITEM_ITEM)
	{
		$("#ButtonInsert").html("插入項目");
		$("#ButtonDelete").html("刪除項目");
	}else if(NowSelected.node.original.type == TREEITEM_PAGE)
	{
		$("#ButtonInsert").html("插入頁面");
		$("#ButtonDelete").html("刪除頁面");
	}else if(NowSelected.node.original.type == TREEITEM_SUBITEM)
	{
		$("#ButtonInsert").html("插入子項目");
		$("#ButtonDelete").html("刪除子項目");
	}
	if(NowSelected.node.original.type == TREEITEM_ITEM || NowSelected.node.original.type == TREEITEM_SUBITEM)
	{
		$("#ItemEditArea").children().prop('disabled',false);
	
		var SelectedText = NowSelected.node.original.text;
		var alias, itemtext;
		testvar2=SelectedText.split(delimiter);
				
		if(testvar2.length == 1)
		{
			alias="";
			itemtext=testvar2[0];
		}else if(testvar2.length == 2)
		{
			alias = testvar2[0];
			itemtext=testvar2[1];
		}else
		{
			//AlertMessage("語法錯誤："+linestr);
			//return false;
		}
		alias = StripTabWhite(alias);
		itemtext = StripTabWhite(itemtext);
		$("#itemalias").val(alias);
		$("#itemcontent").val(itemtext);
	}else
	{
		$("#ItemEditArea").children().prop('disabled',true);
	}
	
	var t = GetItemNameOf(NowSelected.node);
	SetSearchBox(t);

	return;
}



function DeleteUid(parent, uid)
{
	/*var target = SearchUid(uid);
	if(target!=null)
	{
		NowTree = jQuery.grep(parent, function(node) 
		{
			return node.uid != uid;
		});
	}
	return parent;*/
}

function DeleteThing()
{
	if(NowSelected.node.original.type == TREEITEM_ITEM)
	{
		DeleteItem();
	}else if(NowSelected.node.original.type == TREEITEM_PAGE)
	{
		DeletePage();
	}else if(NowSelected.node.original.type == TREEITEM_SUBITEM)
	{
		DeleteSubitem();
	}
	RedrawTree();
}

function DeleteItem()
{
	if(NowSelected.node.original.type != TREEITEM_ITEM)
	{
		alert("delete fault");
		return;
	}
	NowTree = DeleteUid(NowTree, NowSelected.node.original.uid);
}

function DeletePage()
{

}

function InsertAfterLocation(parent, sibling, newone)
{
	var index = parent.indexOf(sibling);
	parent.splice(index+1, 0, newone);
}

function InsertThing()
{
	/*if(NowSelected.node.original.type == TREEITEM_ITEM)
	{
		InsertItem();
	}else if(NowSelected.node.original.type == TREEITEM_PAGE)
	{
		InsertPage();
	}else if(NowSelected.node.original.type == TREEITEM_SUBITEM)
	{
		InsertSubItem();
	}*/
	var aray = FindNodeWithUid(NowSelected.node.original.uid)

	if(NowSelected.node.original.type == TREEITEM_ITEM)
	{
		var NewItemNode = CreateBlankItem();
		InsertAfterLocation(NowTree, aray[0], NewItemNode);
	}else if(NowSelected.node.original.type == TREEITEM_PAGE)
	{
		var NewPageNode = CreateBlankPage();
		NewPageNode.parentitem = aray[1].text;
		InsertAfterLocation(aray[1].children, aray[0], NewPageNode);
		
		var NewSubitemNode = CreateBlankSubitem();
		NewSubitemNode.parentitem = NewPageNode.parentitem;
		NewPageNode.children.push(NewSubitemNode);
		
	}else if(NowSelected.node.original.type == TREEITEM_SUBITEM)
	{
		var NewSubitemNode = CreateBlankSubitem();
		NewSubitemNode.parentitem = aray[2].text;
		InsertAfterLocation(aray[1].children, aray[0], NewSubitemNode);
	}
	
	RedrawTree();
}

function CreateBlankItem()
{
	var nowitemnode = {};
	nowitemnode.text = NewItemText;
	nowitemnode.type = TREEITEM_ITEM;
	nowitemnode.uid = UPREFIX+ (++ GlobalId);
	nowitemnode.children = [];
	SetSearchBox(NewItemText);
	
	return nowitemnode;
}

function CreateBlankPage()
{
	var nowpagenode = {};
	nowpagenode.text = NewPageText;
	nowpagenode.type = TREEITEM_PAGE;
	nowpagenode.parentitem = "";
	nowpagenode.uid = UPREFIX+ (++ GlobalId);
	nowpagenode.children = [];
	
	SetSearchBox(NewPageText);
	return nowpagenode;
}

function CreateBlankSubitem()
{
	var nowsubitemnode = {};
	nowsubitemnode.text = "New subitem";
	nowsubitemnode.type = TREEITEM_SUBITEM;
	nowsubitemnode.parentitem = "";
	nowsubitemnode.uid = UPREFIX+ (++ GlobalId);
	
	SetSearchBox(NewSubitemText); 
	return nowsubitemnode;
}





function FindNodeWithUid(uid)
{
	/*
		when node is not found, returns null
		when node is found, an array is returned. Element 0 will be found node, element 1 will be parent, element 2 will be grand parent
	*/
	
	var RetVal = [];
	
	for(var itemiter in NowTree)
	{
		var ItemNode = NowTree[itemiter];
		if(ItemNode.uid == uid)
		{
			RetVal[0] = ItemNode; 
			return RetVal;
		}
		
		for(var pageiter in ItemNode.children)
		{
			var PageNode = ItemNode.children[pageiter];
		
			if(PageNode.uid == uid)
			{
				RetVal[0] = PageNode; 
				RetVal[1] = ItemNode; 
				return RetVal;
			}
		
			for(var subitemiter in PageNode.children)
			{
				var SubitemNode = PageNode.children[subitemiter];
				if(SubitemNode.uid == uid)
				{
					RetVal[0] = SubitemNode; 
					RetVal[1] = PageNode;
					RetVal[2] = ItemNode;
					return RetVal;
				}
			}
		}
	}
	return null;
}

function SetNodeText(uid, newtext)
{
	var TheNodeAray = FindNodeWithUid(uid);
	if(TheNodeAray)
	{
		TheNodeAray[0].text = newtext;
	}
	return;
}


function RedrawTree()
{
	$("#MacroTree").empty();
	CreateTheTree();
	setTimeout(function() { SearchTree() },800);
}
function EditThing()
{
	var alias = StripTabWhite($("#itemalias").val());
	var text = StripTabWhite($("#itemcontent").val());
	var rst = text;
	if(alias != "")
	{
		text = alias + delimiter + text;
	}

	SetNodeText(NowSelected.node.original.uid, text);
	SetSearchBox(GetItemNameOf(NowSelected.node));
	
	RedrawTree();
}

function SearchTree()
{
	var v = $('#search').val();
	$('#MacroTree').jstree(true).search(v);
}

function SetSearchBox(text)
{
	$('#search').val(text);
}

function SaveEdit()
{
	var r = confirm("確定儲存？");
	if(r == false)
	{
		return;
	}
	var NewMacro = TreeToRaw();
	
	$.ajax(
	{
		url: "setmacro.php",
		data: 
		{
			username: $("#username").val(),
			macroname: $("#macroname").val(),
			macro: NewMacro,
			pwd: $("#pwd").val()
		},
		type: "POST",
		success: function(data,textStatus,jqXHR) 
		{
			alert("save success!")
			
			RedrawTree();
		},
		error: function(data, errorThrown)
		{
			alert("save error")
		},
		complete: function() 
		{
		//
		}
	});
}