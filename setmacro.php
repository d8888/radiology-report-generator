<?php
/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
	require_once("misc.php");

	//var_dump($_POST);
	if(isset($_POST["macro"]))
	{
		if($_POST["macro"]=="" || $_POST["username"]=="" || $_POST["macroname"]=="" || $_POST["pwd"]=="")
		{
			print("使用者名稱、巨集名稱、巨集內容、密碼都不得為空白！");
			exit(-1);
		}
		
		if(!AuthUser($_POST["username"], $_POST["pwd"]))
		{
			print("使用者驗證失敗！");
			exit(-1);
		}
		
		$fname = "files/".md5($_POST["username"].$_POST["macroname"]);
		
		$handle = fopen($fname, "w+");
		if($handle == FALSE)
		{
			print("檔案開啟失敗！");
			exit(-1);
		}
		$_POST["macro"] = str_replace("\r\n", "\n", $_POST["macro"]);
		fwrite($handle, $_POST["macro"]);
		fclose($handle);
		echo "username:".$_POST["username"];
		echo "macroname:".$_POST["macroname"];
		echo "filename:".$fname;
		print("寫入成功！");
	}
?>



<head>
<title>報告巨集維護</title>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<style>
#wrapper {
  margin-right: 600px;
}
#content {
  float: left;
  width: 100%;
  
}
#sidebar {
  float: right;
  width: 600px;
  margin-right: -600px;
  
}
#cleared {
  clear: both;
}
</style>
<script src="jquery.js"></script> 
<script src="ErrObj.js"></script> 
<script src="basefunc.js"></script> 
<script src="setmacro.js"></script> 
</head>


<body onload="initeditor()">
<div id="wrapper">
  <div id="content">
	<form method="post">
		<div>
			使用者名稱：<input type="text" name="username">密碼：<input type="password" name="pwd"><br>
			巨集名稱：<input type="text" name="macroname"><br>
			<textarea name="macro" id="macro" rows="40" cols="50"></textarea>
		</div>
		<input type="submit" value ="送出" onclick="SendReport()">
	</form>
  </div>
  <div id="sidebar">
	<div name="preview" id="preview">預覽</div>
  </div>
	
  <div id="cleared"></div>
</div>
	
</body>