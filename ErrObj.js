ErrObj = [];

function ReportError(errcode, errmsg)
{
	var obj=[];
	obj["errcode"]=errcode;
	obj["errmsg"]=errmsg;
	ErrObj.push(obj);
	return;
}

function ClearError()
{
	ErrObj=[];
}




var ErrCode = {
	AUTH_FAILED: -1,
	SUBITEM_WITHOUT_KEY: -2,
	CYCLE_INLINE:-3,
	UNDEFINED_SUBITEM: -4
}
