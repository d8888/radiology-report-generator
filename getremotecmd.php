<?php
/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
	require_once("misc.php");

	if(!isset($_GET["username"]) || 
	   !isset($_GET["token"]))
	{
		die("Incomplete parameter");
	}
	
	$time = 0;
	while($time < 20)
	{		
		$fname = "files/".GetHashedPwd("Comet".$_GET["username"].$_GET["token"]);
		//echo "debug:~~"."Comet".$_GET["username"].$_GET["token"]."~~<br>";
		$handle = @fopen($fname, "r");
		if($handle===false)
		{
			//echo "cmd not found<br>";
			continue;
		}
		
		while($str=fgets($handle))
		{
			$aray = explode("###",$str);
			$cmd = $aray[0];
			$para = $aray[1];
			
			if(strcmp($cmd,"open")==0)
			{
				echo "open###";
				echo $para;
				fclose($handle);
				unlink($fname);
				exit(0);
			}else
			{
				echo "error###cmd is $cmd and para is $para <br>";
				exit(0);
			}
		}
		sleep(1);
		$time ++;
	}
	
	
?>	