<!DOCTYPE html>
<html lang="en">
<head>
  <title>乳房病灶比較</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="http://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <!-- xregexp -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xregexp/3.2.0/xregexp-all.js"></script>

<script>
    
$(document).ready(function (e)
{
	var regBreastLesion = /((Lt|Rt)\s*\(\s*\d+\s*\/\s*\d+\s*\))\s*(\d+(\.\d*)?\s*((\*|x)\s*\d+(\.\d*)?\s*)+)/g

	function FilterSpace(input)
	{
		return input.replace(/\s+/g, '');
	}

	function FilterSu(input)
	{
		// remove all ( )
		const regPar = XRegExp(/\(|\)/g);

		// left -> Lt, right -> Rt
		const regLtRt = XRegExp(/((L|l)eft|(R|r)ight)/g);

		// ... - 5/5 --> ... (5/5)
		const regLoc = XRegExp(/-?\s*(\d+\s*\/\s*\d+\s*)/g);
			
		// 1.2 x 3.4 x 4.5 --> 1.2*3.4*4.5
		const regTime = XRegExp(/(\d+(\.\d+)?)\s*x/g);

		ret = input;

		ret = XRegExp.replace(ret, regPar, (full) =>
		{
			return ' ';
		});
		console.log("check1");
		console.log(ret);

		ret = XRegExp.replace(ret, regLtRt, (full, a, b) => 
		{
			var side = (a.toLowerCase() == 'left') ? 'Lt' : 'Rt';
			return `${side}`;
		});
		console.log("check2");
		console.log(ret);
		
		ret = XRegExp.replace(ret, regLoc, (full, a) =>
		{
			//console.log("replace"+a+" to:"+`(${a})`);
			return `(${a})`;
		});
		console.log("check 3");
		console.log(ret);


		ret = XRegExp.replace(ret, regTime, (match, a, b) =>
		{
			return `${FilterSpace(a)}*`;
		});
		console.log("check 4");
		console.log(ret);

		ret = ret.replace(/-/g, '');

		console.log("after filter su:");
		console.log(ret);
		return ret;
	}

	function ListLesions(input)
	{
		input = FilterSu(input);
		ret = {};
		do {
			m = regBreastLesion.exec(input);
			if (m)
			{
				console.log(m);
				newk = FilterSpace(m[1]);
				newv = m[3].trim()

				while (ret.hasOwnProperty(newk))
				{
					newk = newk + "*";
				}
				ret[newk] = newv;
			}
		} while (m);
		console.log(ret);
		return ret;
	}

	function ListAllLocs(input)
	{
		var regBreastLoc = /((Lt|Rt)\s*\(\s*\d+\s*\/\s*\d+\s*\))/g
		input = FilterSu(input);
		ret = {};
		do {
			m = regBreastLoc.exec(input);
			if (m)
			{
				console.log(m);
				newk = FilterSpace(m[1]);

				while (ret.hasOwnProperty(newk))
				{
					newk = newk + "*";
				}
				ret[newk] = 1;
			}
		} while (m);
		console.log(ret);
		return ret;
	}

	$("#calculatebutton").click(function ()
	{
		//clear old content
		$('#comparetable').empty();
		$('#comparetable')
			.html("<tbody><tr><td> 病灶 </td><td> 本次大小 </td><td> 上次大小 </td></tr></tbody>");


		//get lesions
		newlesions = ListLesions($('#newreport').val());
		oldlesions = ListLesions($('#oldreport').val());
		oldbreastlocs = ListAllLocs($('#oldreport').val());
		console.log("new lesions:");
		console.log(newlesions);
		console.log("old lesions:");
		console.log(oldlesions);
		console.log("all locations in old lesions:");
		console.log(oldbreastlocs);


		for (var key in newlesions)
		{
			console.log("key is:" + key)
			if (newlesions.hasOwnProperty(key))
			{
				//position
				var str = "<td>" + key + "</td>";
				//size this time
				str += "<td>" + newlesions[key] + "</td>";
				//size last time, if present
				if (oldlesions.hasOwnProperty(key))
				{
					str += "<td>" + oldlesions[key] + "</td>";
				}
				else
				{
					//not found
					if (oldbreastlocs.hasOwnProperty(key))
					{
						str += "<td>??</td>";
					}
					else
					{
						str += "<td>--</td>";
					}
				}
				$('#comparetable tr:last').after("<tr>" + str + "</tr>");
			}

		}


		for (var key in oldlesions)
		{
			if (!newlesions.hasOwnProperty(key))
			{
				//position
				var str = "<td>" + key + "</td>";
				//size this time, which is not found
				str += "<td>--</td>";
				//size last time, if present
				if (oldlesions.hasOwnProperty(key))
				{
					str += "<td>" + oldlesions[key] + "</td>";
				}
				else
				{
					//not found
					str += "<td>--</td>";
				}
				$('#comparetable tr:last').after("<tr>" + str + "</tr>");
			}
		}
	});
});

</script>
    
</head>
<body>

<div class="container">
  <h2>乳房病灶比較</h2>
  <p>乳房病灶格式：<br>
        Rt(1/2)  5.1*5.1 <br>
        Rt(2/3) 3.0*3.0 <br>
        ... <br>
        輸出結果：-- 代表上次沒這個病灶 ?? 代表上次這位置有提到病灶但不一定是當作 nodule  <br>
        一個病灶是否為 nodule 根據位置後面有沒有 2 個徑以上的大小來判定 <br>
        (e.g Rt(1/2) 2mm 會被系統當成 cyst 或鈣化，Rt(1/2)3*3mm 則會被系統當成 nodule) <br>
    
    </p>
  
  <form>
    <div class="form-group">
        <label for="newreport">這次報告</label>
        <textarea class="form-control" rows="6" id="newreport"></textarea>
        <label for="oldreport">上次報告</label>
        <textarea class="form-control" rows="6" id="oldreport"></textarea>
        <button type="button" class="btn btn-outline-primary" id="calculatebutton">計算</button>
    </div> 
            
    <div id="tablediv">
        <table class="table table-dark" id="comparetable">
            <tbody>
<tr><td> 病灶 </td><td> 本次大小 </td><td> 上次大小 </td></tr>
            </tbody>
        </table>
    </div>
        
  </form>
          
          
</div>

</body>
</html>
