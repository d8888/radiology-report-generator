<?php
	$authfile = "files/userlist";
	$algo = "sha256";
	$salt = "ilove5566verymuch!";
	
	
	function ReadAuthFile(&$content)
	{
		global $authfile;
		$handle = @fopen($authfile,"a+");
		if($handle === false)
		{
			trigger_error("ReadAuthFile::Reading auth file failed.", E_USER_ERROR);
			return false;
		}
		$content = @fread($handle, filesize($authfile));
		fclose($handle);
		return true;
	}
	
	function IsUsernameAvailable($username)
	{
		if(ReadAuthFile($content) === false)
		{
			return false;
		}
		
		$aray = explode("\n", $content);
		foreach ($aray as $line)
		{
			$temp = explode("::", $line);
			if(count($temp)!=2)
			{
				continue;
			}
			
			$uname = $temp[0];
			$hashedpwd = $temp[1];
			
			if($username == $uname)
			{

				return false;
			}
		}
		return true;
	}
	
	function GetHashedPwd($pwd)
	{
		global $salt, $algo;
		return hash($algo, $salt.$pwd);
	}
	
	function AddUser($username, $pwd)
	{
		/*
			This function will not check if username is duplicated;
		*/
		if(ReadAuthFile($content) === false)
		{
			return false;
		}
		
		$newline = $username."::".GetHashedPwd($pwd);
		
		global $authfile;
		$handle = fopen($authfile,"a+");
		if($handle === false)
		{
			return false;
		}
		
		if(strlen($content)>0)
		{
			fwrite($handle, "\n");
		}
		fwrite($handle, $newline);
		return true;
	}
	
	function AuthUser($username, $pwd)
	{
		if(ReadAuthFile($content) === false)
		{
			return false;
		}
		
		$aray = explode("\n", $content);
		
		$hasheduserpwd = GetHashedPwd($pwd);
		
		foreach ($aray as $line)
		{
			$temp = explode("::", $line);
			$uname = $temp[0];
			$hashedpwd = $temp[1];
			
			if($username == $uname && $hasheduserpwd == $hashedpwd)
			{
				return true;
			}
		}
		return false;
	}
?>