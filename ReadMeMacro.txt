範例文件請參考附帶的 Webform.txt

巨集是由許多長得像這樣的段落組成

Hello::      <-- 請注意是兩個引號，既不是一個，也不是三個
	A
	B
	C
	
功能是，在填妥正確的使用者名稱，密碼，巨集名稱以後。「顯示項目」填寫 Hello 按下載入，則畫面右方會顯示 A，B，C 三個按鈕可以點，點了分別就能輸入 A，B，C

1. 暱稱

有時候想加的東西很長，比如

Hello::
	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	B
	C
	
如果「AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA」全顯示在按鈕上，畫面會亂。這時可以在前方加上「文字::」做修飾，比如

Hello::
	ManyA::AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	B
	C
	
這樣畫面就會顯示「ManyA」而不是一堆 A，就不會亂。但按下去輸入的一樣是 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA。

2. 連結

Hello::
	OA knee::OA change of {Direction} knee is noted.
	
Direction::
	left
	right
	bilateral
	
系統讀到用兩個中括號 {} 包起來的字的時候，會自動搜尋有沒有哪個項目的名稱，剛好是跟中括號包起來的字一樣的。如果有，系統就會把那個項目展開，然後讓使用者輸入，輸入後的結果再取代掉中括號及中間的文字。

以以上範例來說，當使用者點下 OA knee 的時候，系統會打開 Direction 這個項目，使用者選完以後，把輸入結果拿來替換掉 {Direction} 。

比如使用者選了 left，那系統就會用 left 替換 {Direction}，系統輸入的就會是 OA change of left knee is noted.。如果使用者點 bilateral，系統就會輸入 OA change of bilateral knee is noted.

3. 逗號
	如果某個子項目尾巴是逗號（,）。那麼接下來輸入的東西就只會被逗號隔開，中間不會有空行

比如：
	
Hello::
	A,
	B,
	C,

使用者在三個按鈕各按一下，得到的結果會是
	A, B, C
不會是
	A,
	B,
	C,
	
4. 分頁

如果一個頁面塞滿太多按鈕，可以用「--Page--」符號來分割。請注意拼字大小寫要一樣。比如

Hello::
	A
	B
	--Page--
	C

這樣 Hello 這個項目就會被切成兩頁，第一頁有 A，B。第二頁有 C