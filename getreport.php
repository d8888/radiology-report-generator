<?php
/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
	function dbgprint($text)
	{
		if(isset($_GET["debug"]))
		{
			echo "debug:".$text."<br>";
		}
	}

	if(isset($_POST["username"]))
	{
		$username = $_POST["username"];
	}else if(isset($_GET["username"]))
	{
		$username = $_GET["username"];
	}
	
	dbgprint("username is:".$username);
	
	if(isset($username))
	{		
		$fname = "files/".md5("###".$username);
		
		$handle = fopen($fname, "r");
		dbgprint("filename is:".$fname);
		if($handle == FALSE)
		{
			$rst = "檔案開啟失敗！";
			dbgprint($rst);
		}else
		{
			$rst = fread($handle, filesize($fname));
			fclose($handle);
			
			if($rst == FALSE)
			{

				$rst = "檔案讀取失敗！";
			}else
			{
				// $rst = utf8_encode($rst); //No need as $rst is already base64 encoded
			}
		}
		//echo chr(0xef).chr(0xbb).chr(0xbf); //BOM marker. For Autohotkey and Chinese
		echo base64_decode($rst);
		// echo $rst;
	}
?>