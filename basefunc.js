/*
d8888 Tablet report inputer.
Copyright (C) 2015  d8888 email:smallt@gmail.com

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
	Consts
*/
var PageDelimiter = "--Page--";
var delimiter = "::";
var InlineAray; // used during expanding inline macros, check whether cycling occured
/*
	commaoperator and concatoperator must be exactly one char
	Used to merge different words together without intervening new line
	e.g:
		Macro:
			A|
			B|
		Clicked A, B --> result is AB
		
		Macro:
			A
			B
		Clicked A, B --> result is
			A
			B
*/
var commaoperator = ",";
var concatoperator = "|";




function StripReturn(str)
{
	return String(str).replace(/^(\n+)|(\n+)$/g, '');
}
function StripTabWhite(str)
{
	return String(str).replace(/^(( |\t)+)|(( |\t)+)$/g, '');
}
function LStripTabWhite(str)
{
	return String(str).replace(/^(( |\t)+)/g, '');
}
function RStripTabWhite(str)
{
	return String(str).replace(/(( |\t)+)$/g, '');
}





function IsInlineMacro(target,ret)
{
	var RegEx = /^@@[0-9a-zA-Z ]*$/
	//alert("IsInlineMacro: target is "+target);
	
	m = target.match(RegEx);
	if(m!=null)
	{
		ret["str"]=m[0].slice(2);
		
		return true;
	}
	return false;
}

function MatchNewKey(line, rstobj)
{
	var RegEx = /^[0-9a-zA-Z ]*::$/
	if(line.match(RegEx) != null)
	{
		rstobj.keyname = line.slice(0,-2);
		return true;
	}
	return false;
}

function MatchPageDelimiter(line)
{
	if(line == PageDelimiter)
	{
		return true;
	}
	return false;
}


function InitDictByStr(dictstr, rstobj)
{
	/*
		rstobj.dictobj --> result dictionary array
	*/
	
	
	var NowKey="";
	var MyDict=[];
	var splitdict= dictstr.split("\n");

	for(var key in splitdict)
	{
		line = splitdict[key]
		line = StripTabWhite(line);
		line = StripReturn(line);
		
		if(line=="##AUTHFAILED")
		{
			ReportError(ErrCode["AUTH_FAILED"],"登入失敗！");
			return false;
		}
		
		/*
			Is it a new key?
		*/
		robj = {};
		if(MatchNewKey(line, robj))
		{
			var NowKey = robj.keyname;
			MyDict[NowKey]=[];
			MyDict[NowKey].push([]);
			continue;
		}

		
		/*
			Not new key
			skip if blank line
		*/
		if(line=="")
		{
			continue;
		}
		
		/*
			defining a subitem, however NowKey is blank
			there is bug in grammar!
		*/
		if(NowKey == "" )
		{
			var errmsg ="Grammar error: Subitem "+line+" encountered before defining enclosing keys";
			ReportError(ErrCode["SUBITEM_WITHOUT_KEY"], errmsg);
			continue;
		}
				
		/*
			Default cases
		*/
		if(!MatchPageDelimiter(line))
		{
			MyDict[NowKey][ MyDict[NowKey].length -1 ].push(line);
		}else
		{
			MyDict[NowKey].push([]);
		}
				
	}
	
	/*
		Expand all inline macros
	*/
	for(var key in MyDict)
	{
		for(var page in MyDict[key])
		{
			araytoexpand=[]
			
			for(var item in MyDict[key][page])
			{
				temp=[];
				b=IsInlineMacro(MyDict[key][page][item], temp)
				if(b)
				{
					aray = ExpandInline(temp["str"], MyDict);
					araytoexpand = araytoexpand.concat(aray);
				}else
				{
					araytoexpand.push(MyDict[key][page][item]);
				}
			}
			MyDict[key][page] = araytoexpand;
		}
	}
	rstobj.dictobj=MyDict;

	
	return true;
}

function ExpandInline(target, dictobj)
{
	InlineAray=[]
	return ExpandInline_internal(target, dictobj);
}


function ExpandInline_internal(target, dictobj)
{
	var retval=[];
	
	if(InlineAray[target])
	{
		var errmsg="ExpandInline_internal: cycling is detected when expanding macro "+target;
		ReportError(ErrCode["CYCLE_INLINE"], errmsg);
		return [];
	}
	
	if (typeof dictobj[target] === 'undefined')
	{
		var errmsg="ExpandInline_internal: inline macro key undefined: "+target;
		ReportError(ErrCode["UNDEFINED_SUBITEM"], errmsg)
		return [];
	}
	
	InlineAray[target]=true;
	
	for(var page in dictobj[target])
	{
		for(var key in dictobj[target][page])
		{
			temp=[];
			//alert("ExpandInline_internal Calling IsInlineMacro on:"+target+" "+page+" "+key+" which is "+ MyDict[target][page][key]);
			var b=IsInlineMacro(dictobj[target][page][key], temp);
			if(b)
			{
				rkey = temp["str"];
				//alert("rkey is:"+rkey);
				aray = ExpandInline_internal(rkey, dictobj);
				if(aray.length==0)
				{
					return [];
				}
				retval = retval.concat(aray);
			}else
			{
				retval.push(dictobj[target][page][key]);
			}
		}
	}
	return retval;
}



function RequestRemoteMacro(username, pwd, target, succallback, failcallback)
{
	 $.ajax(
	 {
		url: "getmacro.php",
		data: 
		{
			username: username,
			macroname: target,
			pwd: pwd
		},
		type: "POST",
		dataType: "json",
		success: function(data,textStatus,jqXHR) 
		{
			succallback(data);
		},
		error: function(data, errorThrown)
		{
			failcallback(errorThrown);
		},
		complete: function() 
		{
		//
		}
	}
	);
	

}
